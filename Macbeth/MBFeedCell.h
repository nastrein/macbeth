//
//  MBFeedCell.h
//  Macbeth
//
//  Created by Nolan Astrein on 3/4/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBFeedCell : UITableViewCell

- (void)setUpVoteStatusLabels;

@property (weak, nonatomic) IBOutlet UILabel *firstSentenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *participantMark;
@property (weak, nonatomic) IBOutlet UILabel *upLabel;
@property (weak, nonatomic) IBOutlet UILabel *downLabel;

@end
