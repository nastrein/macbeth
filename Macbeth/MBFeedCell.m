//
//  MBFeedCell.m
//  Macbeth
//
//  Created by Nolan Astrein on 3/4/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBFeedCell.h"

@implementation MBFeedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setUpVoteStatusLabels
{
    [self.upLabel setText:@"\uf0d8"];
    [self.upLabel setFont:[UIFont fontWithName:@"fontawesome" size:35.]];
    [self.upLabel setTextColor:[UIColor greenColor]];
    [self.upLabel setBackgroundColor:[UIColor clearColor]];
    [self.upLabel setHidden:YES];
    
    [self.downLabel setText:@"\uf0d7"];
    [self.downLabel setFont:[UIFont fontWithName:@"fontawesome" size:35.]];
    [self.downLabel setTextColor:[UIColor redColor]];
    [self.downLabel setBackgroundColor:[UIColor clearColor]];
    [self.downLabel setHidden:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
