//
//  MBFeedViewController.h
//  Macbeth
//
//  Created by Nolan Astrein on 2/28/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBFeedViewController : UIViewController

- (id)init;

@end
