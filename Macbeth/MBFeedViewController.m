//
//  MBFeedViewController.m
//  Macbeth
//
//  Created by Nolan Astrein on 2/28/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBFeedViewController.h"
#import "MBNetworkController.h"
#import "MBPartialStoryViewController.h"
#import "MBStory.h"
#import "MBPartialStory.h"
#import "MBFeedCell.h"
#import "MBStoryCell.h"
#import "MBAppDelegate.h"
#import "EGORefreshTableHeaderView.h"
#import "REMenu.h"
#import <QuartzCore/QuartzCore.h>

static const CGFloat kNavbarWidth = 320.;
static const CGFloat kNavbarHeight = 44.;
static const CGFloat kNavbarItemSize = 44.;
static const CGFloat kStatusBarHeight = 20.;

@interface MBFeedViewController ()
<UITableViewDelegate, UITableViewDataSource, MBPartialStroyViewControllerDelegate, EGORefreshTableHeaderDelegate>
{
@private
    CATransition *transition;
    
    /* EGO pull to refresh */
    EGORefreshTableHeaderView *refreshHeaderView;
	BOOL reloading;
}

/* view controller methods */
- (void)viewDidLoad;
- (void)reloadFeedWithFilter:(FilterType)filter Hud:(BOOL)hudFlag;
- (void)promptPartialStoryViewController;
- (void)promptFilterOptions;

/* MBPartialStroyViewControllerDelegate methods */
 - (void)didDismissPartialStoryViewController;

/* UITableView delegate methods */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

/* data source loading/reloading methods */
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

/* UIScrollViewDelegate methods */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

/* EGORefreshTableHeaderDelegate methods */
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view;
- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view;

- (void)didReceiveMemoryWarning;

@property (strong, nonatomic) UITableView *feedTableView;
@property (strong, nonatomic) NSMutableArray *storiesArray;
@property (strong, nonatomic) UILabel *emptyFeedLabel;
@property (assign, nonatomic) int currentCellIndex;

@property (strong, nonatomic) UIView *navbarView;
@property (strong, nonatomic) UIButton *writeButton;
@property (strong, nonatomic) UIButton *filterButton;
@property (strong, nonatomic) UILabel *navbarLabel;

@property (strong, nonatomic) REMenu *menu;
@property (assign, nonatomic) FilterType currentFilterType;

@property (strong, nonatomic) MBPartialStoryViewController *partialStoryViewController;

@end

@implementation MBFeedViewController

- (id)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

#pragma mark - view controller methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    
    self.storiesArray = [[NSMutableArray alloc] init];
    
    [self setCurrentCellIndex:-1];
        
    /* set up navbar */
    self.navbarView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., kNavbarWidth, kNavbarHeight)];
    [self.navbarView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar"]]];
    [self.view addSubview:self.navbarView];
    
    self.writeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.writeButton setFrame:CGRectMake(5., 0., kNavbarItemSize, kNavbarItemSize)];
    [self.writeButton setBackgroundColor:[UIColor clearColor]];
    [self.writeButton.titleLabel setShadowColor:[UIColor blackColor]];
    [self.writeButton.titleLabel setShadowOffset:CGSizeMake(0., 2.)];
    [self.writeButton setTitle:@"\uf044" forState:UIControlStateNormal];
    [self.writeButton.titleLabel setFont:[UIFont fontWithName:@"fontawesome" size:30.]];
    [self.writeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.writeButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [self.writeButton addTarget:self action:@selector(promptPartialStoryViewController) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.writeButton];
    
    self.filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.filterButton setFrame:CGRectMake(screenWidth - kNavbarHeight - 5., 0., kNavbarItemSize, kNavbarItemSize)];
    [self.filterButton setBackgroundColor:[UIColor clearColor]];
    [self.filterButton.titleLabel setShadowColor:[UIColor blackColor]];
    [self.filterButton.titleLabel setShadowOffset:CGSizeMake(0., 2.)];
    [self.filterButton setTitle:@"\uf0b0" forState:UIControlStateNormal];
    [self.filterButton.titleLabel setFont:[UIFont fontWithName:@"fontawesome" size:30.]];
    [self.filterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.filterButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [self.filterButton addTarget:self action:@selector(promptFilterOptions) forControlEvents:UIControlEventTouchUpInside];
    [self.navbarView addSubview:self.filterButton];
    
    self.navbarLabel = [[UILabel alloc] init];
    [self.navbarLabel setFrame:CGRectMake((screenWidth - 150.)/2, 10., 150., 40)];
    [self.navbarLabel setTextAlignment:NSTextAlignmentCenter];
    [self.navbarLabel setBackgroundColor:[UIColor clearColor]];
    [self.navbarLabel setText:@"Macbeth"];
    [self.navbarLabel setFont:[UIFont fontWithName:@"Zapfino" size:20.]];
    [self.navbarLabel setTextColor:[UIColor whiteColor]];
    [self.navbarView addSubview:self.navbarLabel];
    
    /* set up filter menu */
    REMenuItem *recentItem = [[REMenuItem alloc] initWithTitle:@"RECENT"
                                                         image:[UIImage imageNamed:@"Icon_Explore"]
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            [self reloadFeedWithFilter:RECENT_FILTER Hud:NO];
                                                            [self setCurrentFilterType:RECENT_FILTER];
                                                        }];
    
    REMenuItem *bestItem = [[REMenuItem alloc] initWithTitle:@"BEST"
                                                       image:[UIImage imageNamed:@"Icon_Activity"]
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          [self reloadFeedWithFilter:BEST_FILTER Hud:NO];
                                                          [self setCurrentFilterType:BEST_FILTER];
                                                      }];
    
    REMenuItem *userItem = [[REMenuItem alloc] initWithTitle:@"ME"
                                                       image:[UIImage imageNamed:@"Icon_Profile"]
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          [self reloadFeedWithFilter:ME_FILTER Hud:NO];
                                                          [self setCurrentFilterType:ME_FILTER];
                                                      }];
    
    self.menu = [[REMenu alloc] initWithItems:@[recentItem, bestItem, userItem]];
    [self.menu setCornerRadius:4];
    [self.menu setShadowRadius:4];
    [self.menu setShadowColor:[UIColor blackColor]];
    [self.menu setShadowOffset:CGSizeMake(0, 1)];
    [self.menu setShadowOpacity:1];
    [self.menu setImageOffset:CGSizeMake(5, -1)];
    
    /* set up table view controller */
    self.feedTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., kNavbarHeight, screenWidth, screenHeight - kNavbarHeight) style:UITableViewStylePlain];
    [self.feedTableView setDelegate:self];
    [self.feedTableView setDataSource:self];
    [self.view addSubview:self.feedTableView];
    
    /* set up empty feed label */
    self.emptyFeedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., kNavbarHeight, screenWidth, screenHeight - kNavbarHeight)];
    [self.emptyFeedLabel setBackgroundColor:[UIColor whiteColor]];
    [self.emptyFeedLabel setText:@"Contribute to a story fool!"];
    [self.emptyFeedLabel setNumberOfLines:3];
    [self.emptyFeedLabel setFont:[UIFont fontWithName:@"STHeitiTC-Medium" size:35.]];
    [self.emptyFeedLabel setTextAlignment:NSTextAlignmentCenter];
    
    transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    
    /* pull to refresh methods */
    if (refreshHeaderView == nil) {
		refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.,
                                                                                        -self.feedTableView.bounds.size.height,
                                                                                        screenWidth,
                                                                                        self.feedTableView.bounds.size.height)];
		[refreshHeaderView setDelegate:self];
		[self.feedTableView addSubview:refreshHeaderView];
	}
    
    [self reloadFeedWithFilter:RECENT_FILTER Hud:YES];
    [self setCurrentFilterType:RECENT_FILTER];
}

- (void)reloadFeedWithFilter:(FilterType)filter Hud:(BOOL)hudFlag {
    [self.emptyFeedLabel removeFromSuperview];
    [[MBNetworkController getInstance] requestStoriesWithStartNum:0
                                                    andFilterType:filter
                                                        onSuccess:^(NSArray *stories) {
                                                            if([stories count] == 0) {
                                                                [self.view addSubview:self.emptyFeedLabel];
                                                            }
                                                            
                                                            [self.storiesArray setArray:stories];
                                                            [self.feedTableView reloadData];
                                                            [self.feedTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                                                            [self doneLoadingTableViewData];
                                                        }
                                                         onFailue:^{
                                                             [self.feedTableView reloadData];
                                                             [self doneLoadingTableViewData];
                                                         }
                                                          withHud:hudFlag];
}

- (void)promptPartialStoryViewController
{
    void (^transitionToPartialStoryViewController)() = ^() {
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        self.partialStoryViewController = [[MBPartialStoryViewController alloc] init];
        [self.partialStoryViewController setDelegate:self];
        [self presentViewController:self.partialStoryViewController animated:NO completion:nil];
    };
    
    if([self.menu isOpen]) {
        [self.menu closeWithCompletion:transitionToPartialStoryViewController];
    }
    else {
        transitionToPartialStoryViewController();
    }
}

- (void)promptFilterOptions
{
    if ([self.menu isOpen]) {
        return [self.menu close];
     }
    
    [self.menu showFromRect:CGRectMake(0., kNavbarHeight + kStatusBarHeight, self.feedTableView.frame.size.width, self.feedTableView.frame.size.height)
                     inView:[(MBAppDelegate *)[[UIApplication sharedApplication] delegate] window]];
}

#pragma mark - MBPartialStroyViewControllerDelegate methods

- (void)didDismissPartialStoryViewController
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

#pragma mark - UITableView delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.storiesArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [self currentCellIndex]) {
        return self.view.frame.size.height;
    }
    
    return 75.;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.feedTableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    if(indexPath.row == [self currentCellIndex]) {
        [self setCurrentCellIndex:-1];
        [self.feedTableView setScrollEnabled:YES];
        [self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else {
        [self setCurrentCellIndex:indexPath.row];
        [self.feedTableView setScrollEnabled:NO];
        [self.feedTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.feedTableView beginUpdates];
    [self.feedTableView endUpdates];
    [self.feedTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    if(self.currentCellIndex == indexPath.row) {
        MBStoryCell *cell = [[MBStoryCell alloc] init];
        [cell configureCellForStory:self.storiesArray[indexPath.row]];
        return cell;
    }
    else {
        static NSString *cellIdentifier = @"MBFeedCell";
        MBFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil) {
            NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBFeedCell" owner:self options:nil];
            cell = [nibs objectAtIndex:0];
            [cell.firstSentenceLabel setText:[self.storiesArray[indexPath.row] firstSentence]];
            [cell.scoreLabel setText:[NSString stringWithFormat:@"%d",[self.storiesArray[indexPath.row] score]]];
            
            if([self.storiesArray[indexPath.row] isParticipant]) {
                [cell.participantMark setHidden:NO];
            }
            
            [cell setUpVoteStatusLabels];
            if([self.storiesArray[indexPath.row] voteStatus] == UP_VOTE) {
                [cell.upLabel setHidden:NO];
            }
            else if([self.storiesArray[indexPath.row] voteStatus] == DOWN_VOTE) {
                [cell.downLabel setHidden:NO];
            }
        }
        return cell;
    }
    return nil;
}

#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	[refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	[refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat height = scrollView.frame.size.height;
    
    CGFloat contentYoffset = scrollView.contentOffset.y;
    
    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
    
    if((distanceFromBottom <= height) && !reloading && [self.storiesArray count] != 0) {
        reloading = YES;
        
        NSLog(@"fetching!");
        
        [[MBNetworkController getInstance] requestStoriesWithStartNum:[self.storiesArray count]
                                                        andFilterType:self.currentFilterType
                                                            onSuccess:^(NSArray *stories) {
                                                                [self.storiesArray addObjectsFromArray:stories];
                                                                [self.feedTableView reloadData];
                                                                [self doneLoadingTableViewData];
                                                            }
                                                             onFailue:^ {
                                                                 [self.feedTableView reloadData];
                                                                 [self doneLoadingTableViewData];
                                                             }
                                                              withHud:NO];
    }
}

#pragma mark data source loading/reloading methods

- (void)reloadTableViewDataSource
{
    [self reloadFeedWithFilter:self.currentFilterType Hud:NO];
	reloading = YES;
}

- (void)doneLoadingTableViewData
{
	reloading = NO;
	[refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.feedTableView];
}


#pragma mark EGORefreshTableHeaderDelegate methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	[self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	return reloading;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
