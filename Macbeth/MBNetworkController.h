//
//  MBNetworkController.h
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBStory.h"
#import <Foundation/Foundation.h>

@class MBPartialStory;

typedef enum _FilterType : unsigned int {
    BEST_FILTER = 0,
    RECENT_FILTER = 1,
    ME_FILTER = 2
} FilterType;

@interface MBNetworkController : NSObject

+ (MBNetworkController *)getInstance;

/* feed methods */
- (void)requestStoriesWithStartNum:(int)start
                     andFilterType:(FilterType)filterType
                         onSuccess:(void (^)(NSArray *stories))success
                          onFailue:(void (^)(void))failure
                           withHud:(BOOL)hudFlag;

/* writing methods */
- (void)requestPromptOnSuccess:(void (^)(MBPartialStory *partialStory))success;

- (void)updateStoryWithLine:(NSString *)line
                    storyID:(NSString *)storyID
                  onSuccess:(void (^)(BOOL succeeded))success;

/* voting methods */
- (void)castVoteForStory:(NSString *)storyID
                voteType:(VoteType)voteType
               onSuccess:(void (^)(int score))success;

@end
