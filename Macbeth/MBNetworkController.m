//
//  MBNetworkController.m
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBNetworkController.h"
#import "MBReachability.h"
#import "MBStory.h"
#import "MBPartialStory.h"
#import "ASIFormDataRequest.h"
#import "ASINetworkQueue.h"
#import "SVProgressHUD.h"
#import "MBAppDelegate.h"

static const NSURL *kBaseUrl;
static const int kLimit = 10;

@interface MBNetworkController ()

- (id)init;

/* request helpers */
- (void)sendRequest:(ASIHTTPRequest *)request
       onCompletion:(ASIBasicBlock)completion
          onFailure:(ASIBasicBlock)failed;

- (void)networkFailureAlert;

@property (strong) ASINetworkQueue *requestQueue;

@end

@implementation MBNetworkController

- (id)init
{
    self = [super init];
    if (self) {
        kBaseUrl = [NSURL URLWithString:@"http://macbeth.gauravkulkarni.com"];
        [self setRequestQueue:[ASINetworkQueue queue]];
        [self.requestQueue go];
    }
    return self;
}

+ (MBNetworkController *)getInstance
{
    static MBNetworkController *sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[MBNetworkController alloc] init];
    });
    
    return (sharedInstance);
}

#pragma mark - feed methods

- (void)requestStoriesWithStartNum:(int)start
                     andFilterType:(FilterType)filterType
                         onSuccess:(void (^)(NSArray *stories))success
                          onFailue:(void (^)(void))failure
                           withHud:(BOOL)hudFlag;
{
    NSString *accessPoint = [NSString stringWithFormat:@"/api/stories?start=%d&limit=%d&filter=%d", start, kLimit, filterType];
    
    NSURL *url = [NSURL URLWithString:accessPoint relativeToURL:(NSURL *)kBaseUrl];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"GET"];
    __weak ASIHTTPRequest *wRequest = request;
    
    if(hudFlag) {
        [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    }
        
    [self sendRequest:request onCompletion:^{
        [SVProgressHUD dismiss];
        
        NSError *error;
        NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:[wRequest responseData] options:0 error:&error];
        NSArray *objectsArray = [responseObject objectForKey:@"stories"];
        
        NSMutableArray *storiesArray = [NSMutableArray arrayWithCapacity:[objectsArray count]];
        for (int i = 0; i < [objectsArray count]; ++i) {
            [storiesArray addObject:[[MBStory alloc] initFromDataObject:objectsArray[i]]];
        }
        
        success(storiesArray);
    }
    onFailure:^{
        failure();
        [self networkFailureAlert];
        
        NSError *error = [wRequest error];
        NSLog(@"%@", error);
    }];
}

#pragma mark - writing methods

- (void)requestPromptOnSuccess:(void (^)(MBPartialStory *partialStory))success
{
    NSString *accessPoint = @"/api/partials/random";
    
    NSURL *url = [NSURL URLWithString:accessPoint relativeToURL:(NSURL *)kBaseUrl];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"GET"];
    __weak ASIHTTPRequest *wRequest = request;
    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    
    [self sendRequest:request onCompletion:^{
        [SVProgressHUD dismiss];
        
        NSError *error;
        NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:[wRequest responseData] options:0 error:&error];
        
        MBPartialStory *partialStory = [[MBPartialStory alloc] initFromDataObject:responseObject];
        success(partialStory);
    }
    onFailure:^{
        [self networkFailureAlert];
        NSError *error = [wRequest error];
        NSLog(@"%@", error);
    }];
}

- (void)updateStoryWithLine:(NSString *)line
                    storyID:(NSString *)storyID
                  onSuccess:(void (^)(BOOL succeeded))success
{
    NSString *accessPoint = [NSString stringWithFormat:@"/api/partials/%@", storyID];
    
    NSURL *url = [NSURL URLWithString:accessPoint relativeToURL:(NSURL *)kBaseUrl];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setRequestMethod:@"PUT"];
    __weak ASIFormDataRequest *wRequest = request;
    
    [request setPostValue:line forKey:@"line"];
        
    [self sendRequest:request onCompletion:^{
        success(YES);
    }
    onFailure:^{
        [self networkFailureAlert];
        NSError *error = [wRequest error];
        NSLog(@"%@", error);
    }];
}

#pragma mark - voting methods

- (void)castVoteForStory:(NSString *)storyID
                voteType:(VoteType)voteType
               onSuccess:(void (^)(int score))success
{
    NSString *accessPoint = [NSString stringWithFormat:@"/api/story/%@", storyID];
    
    NSURL *url = [NSURL URLWithString:accessPoint relativeToURL:(NSURL *)kBaseUrl];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setRequestMethod:@"PUT"];
    __weak ASIFormDataRequest *wRequest = request;
    
    [request setPostValue:[NSNumber numberWithInt:voteType] forKey:@"vote"];
    
    [self sendRequest:request onCompletion:^{
        NSError *error;
        NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:[wRequest responseData] options:0 error:&error];
        
        int score = [[responseObject objectForKey:@"score"] intValue];
        success(score);
    }
    onFailure:^{
        [self networkFailureAlert];
        NSError *error = [wRequest error];
        NSLog(@"%@", error);
    }];
}

#pragma mark - request helpers

- (void)sendRequest:(ASIHTTPRequest *)request onCompletion:(ASIBasicBlock)completion onFailure:(ASIBasicBlock)failed
{
    if([MBReachability hasInternetConnection]) {
        NSString *uuid = [(MBAppDelegate *)[[UIApplication sharedApplication] delegate] uuid]; /* fix me */
        
        [request addRequestHeader:@"Accept" value:@"application/vnd.macbeth;version=1"];
        [request addRequestHeader:@"User-ID" value:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
        
        [request setCompletionBlock:completion];
        [request setFailedBlock:failed];
        
        [self.requestQueue addOperation:request];
    }
    else {
        [SVProgressHUD dismiss];
    }
}

- (void)networkFailureAlert
{
    [SVProgressHUD dismiss];
    
    [[[UIAlertView alloc] initWithTitle:@"Aww Snap!"
                                message:@"A network failure occurred."
                               delegate:self
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil] show];
}

@end
