//
//  MBPartialStory.h
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBSerializable.h"
#import <Foundation/Foundation.h>

@interface MBPartialStory : NSObject

@property (strong, nonatomic) NSString *storyID;
@property (strong, nonatomic) NSString *prompt;
@property (assign) int lineNumber;

/* MBSerializable */
- (id)initFromDataObject:(NSDictionary *)object;

@end
