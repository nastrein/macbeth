//
//  MBPartialStory.m
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBPartialStory.h"

const NSString *kPartialStoryIDKey = @"id";
const NSString *kPromptKey = @"prompt";
const NSString *kLineNumberKey = @"line_number";

@implementation MBPartialStory

#pragma mark - MBSerializable

- (id)initFromDataObject:(NSDictionary *)object
{
    if (self = [super init]) {
        [self setStoryID:[object objectForKey:kPartialStoryIDKey]];
        [self setPrompt:[object objectForKey:kPromptKey]];
        [self setLineNumber:[[object objectForKey:kLineNumberKey] intValue]];
    }
    return self;
}

@end
