//
//  MBPartialStoryViewController.h
//  Macbeth
//
//  Created by Nolan Astrein on 3/2/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MBPartialStroyViewControllerDelegate <NSObject>

@required
- (void)didDismissPartialStoryViewController;

@end

@interface MBPartialStoryViewController : UIViewController

- (id)init;

@property (weak, nonatomic) id<MBPartialStroyViewControllerDelegate> delegate;

@end
