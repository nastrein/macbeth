//
//  MBPartialStoryViewController.m
//  Macbeth
//
//  Created by Nolan Astrein on 3/2/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBPartialStoryViewController.h"
#import "MBNetworkController.h"
#import "MBPartialStory.h"
#import "MBTextView.h"
#import <QuartzCore/QuartzCore.h>

static const CGFloat kPromptHeight = 120.;
static const CGFloat kPromptWidth = 280.;
static const CGFloat kDismissButtonSize = 40.;
static const CGFloat kLineNumberLabelWidth = 80.;
static const CGFloat kLineNumberLabelHeight = 35.;
static const CGFloat kSubmitButtonWidth = 100.;
static const CGFloat kSubmitButtonHeight = 30.;
static const CGFloat kCounterWidth = 80.;
static const CGFloat kCounterHeight = 30.;

static const CGFloat kMaxCharacters = 141.;

@interface MBPartialStoryViewController ()
<UITextViewDelegate>
{
@private
    UIView *dummyViewLeft;
    UIView *dummyViewRight;
    UIView *fillerView;
    UIImageView *penImageView;
}

- (void)updateStory;

- (void)sealPageAnimation;
- (void)scrollPaperAnimation;

@property (strong, nonatomic) UIView *whiteLayer;
@property (strong, nonatomic) MBTextView *promptTextView;
@property (strong, nonatomic) UILabel *lineNumberLabel;
@property (strong, nonatomic) UIImageView *tearImageView;

@property (strong, nonatomic) UITextView *lineTextView;
@property (strong, nonatomic) UILabel *characterCountLabel;

@property (strong, nonatomic) UIButton *submitButton;
@property (strong, nonatomic) UIButton *dismissButton;

@property (strong, nonatomic) MBPartialStory *currentPartialStory;

@property (strong, nonatomic) UITapGestureRecognizer *tappedWhiteLayer;

@end

@implementation MBPartialStoryViewController

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    
    UIColor *greyColor = [UIColor colorWithRed:239./255. green:239./255. blue:239./255. alpha:1.0];
    
    [self.view setBackgroundColor:greyColor];
    
    /* set up white layer */
    self.whiteLayer = [[UIView alloc] initWithFrame:CGRectMake(10., 0., screenWidth - 20., screenHeight*2)];
    [self.whiteLayer setBackgroundColor:[UIColor whiteColor]];
    self.whiteLayer.layer.masksToBounds = NO;
    self.whiteLayer.layer.shadowOffset = CGSizeMake(0, 0);
    self.whiteLayer.layer.shadowRadius = 1;
    self.whiteLayer.layer.shadowOpacity = 0.8;
    [self.view addSubview:self.whiteLayer];
    
    /* set up tap gesture recognizer */
    self.tappedWhiteLayer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignLineTextViewResponder)];
    [self.tappedWhiteLayer setNumberOfTapsRequired:1];
    [self.whiteLayer addGestureRecognizer:self.tappedWhiteLayer];
    
    /* set up dummy views */
    dummyViewLeft = [[UIView alloc] initWithFrame:CGRectMake(0., 168., 10., 35.)];
    [dummyViewLeft setBackgroundColor:greyColor];
    [self.view addSubview:dummyViewLeft];
    
    dummyViewRight = [[UIView alloc] initWithFrame:CGRectMake(310., 180., 10., 15.)];
    [dummyViewRight setBackgroundColor:greyColor];
    [self.view addSubview:dummyViewRight];
    
    /* set up prompt text view */
    self.promptTextView = [[MBTextView alloc] initWithFrame:CGRectMake(10., 35., kPromptWidth, kPromptHeight)];
    [self.promptTextView setUserInteractionEnabled:YES];
    [self.promptTextView setEditable:NO];
    [self.promptTextView setFont:[UIFont fontWithName:@"STHeitiTC-Light" size:20.]];
    [self.whiteLayer addSubview:self.promptTextView];

    /* set up dismiss button */
    self.dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.dismissButton setFrame:CGRectMake(5., 2., kDismissButtonSize, kDismissButtonSize)];
    [self.dismissButton setBackgroundColor:[UIColor clearColor]];
    [self.dismissButton.titleLabel setTextAlignment:NSTextAlignmentRight];
    [self.dismissButton.titleLabel setShadowColor:[UIColor blackColor]];
    [self.dismissButton.titleLabel setShadowOffset:CGSizeMake(0., 1.)];
    [self.dismissButton setTitle:@"\uf00d" forState:UIControlStateNormal];
    [self.dismissButton.titleLabel setFont:[UIFont fontWithName:@"fontawesome" size:40.]];
    [self.dismissButton setTitleColor:greyColor forState:UIControlStateNormal];
    [self.dismissButton setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [self.dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.whiteLayer addSubview:self.dismissButton];
    
    /* set up line number label */
    self.lineNumberLabel = [[UILabel alloc] init];
    [self.lineNumberLabel setFrame:CGRectMake(screenWidth - 20. - kLineNumberLabelWidth - 10.,
                                              7.,
                                              kLineNumberLabelWidth,
                                              kLineNumberLabelHeight)];
    [self.lineNumberLabel setTextAlignment:NSTextAlignmentRight];
    [self.lineNumberLabel setFont:[UIFont fontWithName:@"STHeitiTC-Medium" size:30.]];
    [self.whiteLayer addSubview:self.lineNumberLabel];
    
    /* set up tear image view */
    self.tearImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 155., 300., 61.)];
    [self.tearImageView setImage:[UIImage imageNamed:@"pagetear"]];
    [self.whiteLayer addSubview:self.tearImageView];
    
    /* set up filler view */
    fillerView = [[UIView alloc] initWithFrame:CGRectMake(0., 155., 0., 61.)];
    [fillerView setBackgroundColor:[UIColor whiteColor]];
    [self.whiteLayer addSubview:fillerView];
    
    /* set up new line text view */
    self.lineTextView = [[UITextView alloc] initWithFrame:CGRectMake(10., 216., 280., 90.)];
    [self.lineTextView setDelegate:self];
    [self.lineTextView setFont:[UIFont fontWithName:@"STHeitiTC-Light" size:20.]];
    [self.whiteLayer addSubview:self.lineTextView];
    
    /* set up pen image view */
    penImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-10., 200., 85., 54.)];
    [penImageView setImage:[UIImage imageNamed:@"pen"]];
    [self.whiteLayer addSubview:penImageView];
    
    /* set up submit button */
    self.submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.submitButton setFrame:CGRectMake((screenWidth - kSubmitButtonWidth - 20.)/2., 311., kSubmitButtonWidth, kSubmitButtonHeight)];
    [self.submitButton setBackgroundImage:[UIImage imageNamed:@"publish"] forState:UIControlStateNormal];
    [self.submitButton addTarget:self action:@selector(updateStory) forControlEvents:UIControlEventTouchUpInside];
    [self.submitButton setHidden:YES];
    [self.submitButton setEnabled:NO];
    [self.whiteLayer addSubview:self.submitButton];
    
    /* set up counter label */
    self.characterCountLabel = [[UILabel alloc] init];
    [self.characterCountLabel setFrame:CGRectMake(screenWidth - kCounterWidth - 30., 311., kCounterWidth, kCounterHeight)];
    [self.characterCountLabel setTextAlignment:NSTextAlignmentRight];
    [self.characterCountLabel setFont:[UIFont fontWithName:@"STHeitiTC-Light" size:20.]];
    [self.characterCountLabel setTextColor:greyColor];
    [self.characterCountLabel setShadowColor:[UIColor blackColor]];
    [self.characterCountLabel setShadowOffset:CGSizeMake(0., 1.)];
    [self.characterCountLabel setHidden:YES];
    [self.whiteLayer addSubview:self.characterCountLabel];

    [[MBNetworkController getInstance] requestPromptOnSuccess:^(MBPartialStory *partialStory) {
        [self setCurrentPartialStory:partialStory];
        [self.promptTextView setText:[NSString stringWithFormat:@"%@...", partialStory.prompt]];
        [self.promptTextView flashScrollIndicators];

        [self.lineNumberLabel setText:[NSString stringWithFormat:@"#%d", partialStory.lineNumber]];
    }];
}


- (void)updateStory
{
    [self.view setUserInteractionEnabled:NO];
    
    [penImageView removeFromSuperview];
    
    [self sealPageAnimation];
    
    [[MBNetworkController getInstance] updateStoryWithLine:self.lineTextView.text
                                                   storyID:self.currentPartialStory.storyID
                                                 onSuccess:^(BOOL finished) {
                                                     NSLog(@"Story updated");
                                                 }];
}

- (void)sealPageAnimation
{
    [self.lineTextView resignFirstResponder];
    [UIView animateWithDuration:1.
                          delay:.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [fillerView setFrame:CGRectMake(0., 155., 300., 61.)];
                         [dummyViewLeft removeFromSuperview];
                     }
                     completion:^(BOOL finished) {
                         [dummyViewRight removeFromSuperview];
                         [self scrollPaperAnimation];
                     }];
}

- (void)scrollPaperAnimation
{
    CGFloat screenWidth = self.view.frame.size.width;
    CGFloat screenHeight = self.view.frame.size.height;
    
    [UIView animateWithDuration:.5
                          delay:.2
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.whiteLayer setFrame:CGRectMake(10., -screenHeight, screenWidth - 20., screenHeight*2)];
                     }
                     completion:^(BOOL finished) {
                         [self dismiss];
                     }];
}

- (void)dismiss
{
    [self.delegate didDismissPartialStoryViewController];
}

- (void)resignLineTextViewResponder
{
    [self.lineTextView resignFirstResponder];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [UIView animateWithDuration:.25 animations:^(void) {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x,
                                       self.view.frame.origin.y - 100.,
                                       self.view.frame.size.width,
                                       self.view.frame.size.height)];
    }];
    [self.submitButton setHidden:NO];
    [self.characterCountLabel setHidden:NO];
    [penImageView setHidden:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [UIView animateWithDuration:.25 animations:^(void) {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x,
                                       self.view.frame.origin.y + 100.,
                                       self.view.frame.size.width,
                                       self.view.frame.size.height)];
    }];
    [self.submitButton setHidden:YES];
    [self.characterCountLabel setHidden:YES];
    
    if([textView.text isEqualToString:@""])
        [penImageView setHidden:NO];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(textView.text.length < kMaxCharacters || range.length == 1) {
        return YES;
    }
    
    return NO;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self.characterCountLabel setText:[NSString stringWithFormat:@"%d", textView.text.length]];
    
    if([textView.text isEqualToString:@""]){
        [self.submitButton setEnabled:NO];
    }
    else {
        [self.submitButton setEnabled:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
