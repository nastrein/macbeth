//
//  MBReachability.h
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBReachability : NSObject

+ (BOOL)hasInternetConnection;

@end
