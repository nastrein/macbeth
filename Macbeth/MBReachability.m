//
//  MBReachability.m
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBReachability.h"
#import "Reachability.h"

@implementation MBReachability

+ (BOOL)hasInternetConnection
{
    Reachability *r = [Reachability reachabilityForInternetConnection];
    if (![r isReachable]) {
        [[[UIAlertView alloc]initWithTitle:@"Aww Snaps!"
                                   message:@"You are not currently connected to the Internet."
                                  delegate:self
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}

@end
