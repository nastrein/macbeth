//
//  MBStory.h
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBSerializable.h"
#import <Foundation/Foundation.h>

typedef enum _VoteType : unsigned int {
    DOWN_VOTE = -1,
    NO_VOTE = 0,
    UP_VOTE = 1
} VoteType;

@interface MBStory : NSObject <MBSerializable>

@property (strong, nonatomic) NSString *storyID;
@property (strong, nonatomic) NSString *firstSentence;
@property (assign, nonatomic, getter = isParticipant) BOOL participant;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) int score;
@property (assign, nonatomic) VoteType voteStatus;
@property (assign, nonatomic) NSRange participantRange;

/* MBSerializable */
- (id)initFromDataObject:(NSDictionary *)object;

@end
