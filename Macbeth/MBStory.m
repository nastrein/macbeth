//
//  MBStory.m
//  Macbeth
//
//  Created by Nolan Astrein on 2/27/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBStory.h"

const NSString *kStoryIDKey = @"id";
const NSString *kFirstSentenceKey = @"first_sentence";
const NSString *kParticipantKey = @"participant";
const NSString *kTextKey = @"text";
const NSString *kScoreKey = @"score";
const NSString *kVoteStatusKey = @"vote_status";
const NSString *kRangeLocationKey = @"range_location";
const NSString *kRangeLengthKey = @"range_length";

@implementation MBStory

#pragma mark - MBSerializable

- (id)initFromDataObject:(NSDictionary *)object
{
    if (self = [super init]) {
        [self setStoryID:[object objectForKey:kStoryIDKey]];
        [self setFirstSentence:[object objectForKey:kFirstSentenceKey]];
        [self setParticipant:[[object objectForKey:kParticipantKey] boolValue]];
        [self setText:[object objectForKey:kTextKey]];
        [self setScore:[[object objectForKey:kScoreKey] intValue]];
        [self setVoteStatus:[[object objectForKey:kVoteStatusKey] intValue]];
        [self setParticipantRange:NSMakeRange([[object objectForKey:kRangeLocationKey] intValue],
                                              [[object objectForKey:kRangeLengthKey] intValue])];
    }
    return self;
}

@end
