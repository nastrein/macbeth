//
//  MBStoryCell.h
//  Macbeth
//
//  Created by Nolan Astrein on 3/8/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBStory;

@interface MBStoryCell : UITableViewCell

- (void)configureCellForStory:(MBStory *)story;

@end
