//
//  MBStoryCell.m
//  Macbeth
//
//  Created by Nolan Astrein on 3/8/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBStoryCell.h"
#import "MBNetworkController.h"
#import "MBTextView.h"
#import "MBTapGestureRecognizer.h"
#import <QuartzCore/QuartzCore.h>

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

@interface MBStoryCell ()
<MBTapGestureRecognizerDelegate>

@property (strong, nonatomic) MBStory *story;

@property (strong, nonatomic) UITextView *storyTextView;
@property (strong, nonatomic) MBTapGestureRecognizer *tapGestureRecognizer;

@property (strong, nonatomic) UIButton *upVoteButton;
@property (strong, nonatomic) UIButton *downVoteButton;
@property (strong, nonatomic) UILabel *scoreLabel;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation MBStoryCell

- (id)init
{
    self = [super init];
    if (self) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        self.story = [[MBStory alloc] init];
                
        self.storyTextView = [[MBTextView alloc] init];
        [self.contentView addSubview:self.storyTextView];
        
        /* set up tap gesture recognizer */
        self.tapGestureRecognizer = [[MBTapGestureRecognizer alloc] init];
        [self.tapGestureRecognizer setDelegate:self];
        [self.tapGestureRecognizer setNumberOfTapsRequired:1];
        [self.tapGestureRecognizer setNumberOfTouchesRequired:1];
        [self.storyTextView addGestureRecognizer:self.tapGestureRecognizer];
        
        /* set up upvote button */
        self.upVoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.upVoteButton setTitle:@"\uf087" forState:UIControlStateNormal];
        [self.upVoteButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self.upVoteButton.titleLabel setFont:[UIFont fontWithName:@"fontawesome" size:40.]];
        [self.upVoteButton addTarget:self action:@selector(castVote:) forControlEvents:UIControlEventTouchUpInside];
        [self.upVoteButton setTag:UP_VOTE];
        [self.contentView addSubview:self.upVoteButton];
        
        /* set up downvote button */
        self.downVoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.downVoteButton setTitle:@"\uf087" forState:UIControlStateNormal];
        [self.downVoteButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self.downVoteButton.titleLabel setFont:[UIFont fontWithName:@"fontawesome" size:40.]];
        [self.downVoteButton addTarget:self action:@selector(castVote:) forControlEvents:UIControlEventTouchUpInside];
        self.downVoteButton.transform = CGAffineTransformMakeRotation(M_PI);
        [self.downVoteButton setTag:DOWN_VOTE];
        [self.contentView addSubview:self.downVoteButton];
        
        /* set up score label */
        self.scoreLabel = [[UILabel alloc] init];
        [self.scoreLabel setTextAlignment:NSTextAlignmentCenter];
        [self.scoreLabel setAdjustsFontSizeToFitWidth:YES];
        [self.scoreLabel setAdjustsLetterSpacingToFitWidth:YES];
        [self.scoreLabel setFont:[UIFont fontWithName:@"STHeitiTC-Medium" size:40.]];
        [self.contentView addSubview:self.scoreLabel];
        
        /* set up activity view indicator */
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] init];
        [self.activityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [self.activityIndicatorView setHidden:YES];
        [self.activityIndicatorView setHidesWhenStopped:YES];
        [self.contentView addSubview:self.activityIndicatorView];
        
        /* set up ui frames */
        if(IS_IPHONE5) {
            [self setFrame:CGRectMake(0., 0., 320., 568.-10.-44.)];
            [self.storyTextView setFrame:CGRectMake(20., 0., 280.,  428)];
            [self.downVoteButton setFrame:CGRectMake(70., 447., 47., 47.)];
            [self.upVoteButton setFrame:CGRectMake(203., 447., 47., 47.)];
            [self.scoreLabel setFrame:CGRectMake(125., 436., 70., 70.)];
        }
        else {
            [self setFrame:CGRectMake(0., 0., 320., 480.-10.-44.)];
            [self.storyTextView setFrame:CGRectMake(20., 0., 280.,  340.)];
            [self.downVoteButton setFrame:CGRectMake(70., 359., 47., 47.)];
            [self.upVoteButton setFrame:CGRectMake(203., 359., 47., 47.)];
            [self.scoreLabel setFrame:CGRectMake(125., 348., 70., 70.)];
        }
        [self.activityIndicatorView setCenter:self.scoreLabel.center];
        
    }
    return self;
}

- (void)configureCellForStory:(MBStory *)story
{
    [self setStory:story];
    [self.storyTextView setText:[story text]];
    
    NSMutableAttributedString *storyAttributedString = [[NSMutableAttributedString alloc] initWithString:[story text]];
    [storyAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [[story text] length])];
    [storyAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:[story participantRange]];
    [storyAttributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"STHeitiTC-Light" size:25.] range:NSMakeRange(0, [[story text] length])];
    [self.storyTextView setAttributedText:storyAttributedString];
    
    [self.scoreLabel setText:[NSString stringWithFormat:@"%d",[story score]]];
    
    if(story.voteStatus == DOWN_VOTE) {
        [self.downVoteButton setSelected:YES];
        [self.downVoteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    else if(story.voteStatus == UP_VOTE) {
        [self.upVoteButton setSelected:YES];
        [self.upVoteButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    }
}

- (void)castVote:(id)sender
{
    UIButton *voteButton = (UIButton *)sender;
    
    VoteType voteType;
    
    if([voteButton isSelected]) {
        voteType = NO_VOTE;
        [voteButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [voteButton setSelected:NO];
    }
    else if([voteButton tag] == DOWN_VOTE) {
        voteType = DOWN_VOTE;
        [voteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [voteButton setSelected:YES];
        
        [self.upVoteButton setSelected:NO];
        [self.upVoteButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    else {
        voteType = UP_VOTE;
        [voteButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [voteButton setSelected:YES];
        
        [self.downVoteButton setSelected:NO];
        [self.downVoteButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    [self.scoreLabel setHidden:YES];
    [self.activityIndicatorView setHidden:NO];
    [self.activityIndicatorView startAnimating];
    
    [[MBNetworkController getInstance] castVoteForStory:self.story.storyID
                                               voteType:voteType
                                              onSuccess:^(int score){
                                                  [self.story setScore:score];
                                                  [self.story setVoteStatus:voteType];
                                                  [self.scoreLabel setText:[NSString stringWithFormat:@"%d",[self.story score]]];
                                                  
                                                  [self.activityIndicatorView stopAnimating];
                                                  [self.scoreLabel setHidden:NO];
                                              }
     ];
}

- (void)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer endedWithTouches:(NSSet*)touches andEvent:(UIEvent *)event
{
    [self.superview touchesBegan:touches withEvent:event];
    [self.superview touchesEnded:touches withEvent:event];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
