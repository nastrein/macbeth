//
//  MBTapGestureRecognizer.h
//  Macbeth
//
//  Created by Nolan Astrein on 4/7/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIGestureRecognizerSubclass.h>

@protocol MBTapGestureRecognizerDelegate <UIGestureRecognizerDelegate>

- (void)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer endedWithTouches:(NSSet*)touches andEvent:(UIEvent *)event;

@end

@interface MBTapGestureRecognizer : UITapGestureRecognizer

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end
