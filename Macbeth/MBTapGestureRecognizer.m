//
//  MBTapGestureRecognizer.m
//  Macbeth
//
//  Created by Nolan Astrein on 4/7/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBTapGestureRecognizer.h"

@implementation MBTapGestureRecognizer

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [super touchesEnded:touches withEvent:event];
    
    if ([self.delegate respondsToSelector:@selector(gestureRecognizer:endedWithTouches:andEvent:)]) {
        [(id)self.delegate gestureRecognizer:self endedWithTouches:touches andEvent:event];
    }
    
}

@end
