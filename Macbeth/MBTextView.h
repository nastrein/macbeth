//
//  MBPromptTextView.h
//  Macbeth
//
//  Created by Nolan Astrein on 3/5/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBTextView : UITextView

@end
