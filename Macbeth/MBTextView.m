//
//  MBPromptTextView.m
//  Macbeth
//
//  Created by Nolan Astrein on 3/5/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "MBTextView.h"

@implementation MBTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setCanCancelContentTouches:NO];
        [self setDelaysContentTouches:YES];
    }
    return self;
} 

- (BOOL)canBecomeFirstResponder
{
    return NO;
}

@end
